
$(document).ready(function () {
	// empty json object
	var jObject = [];

    //event handler for submit button
    $("#btnSubmit").click(function () {
        //collect userName and password entered by users
        var userName = $("#username").val();
        var password = $("#password").val();
		
		// add to json object
		tempObject = {username: password};
		jObject.push(tempObject);
		localStorage.setItem('jObject', JSON.stringify(jObject));

		// clear input fields
		$("#username").val("");
		$("#password").val("");

		document.getElementById("verify").innerHTML = "Login saved.";

    });

    // event handler for login button
    $("#btnLogon").click(function () {
    	
    	// get input
        var userName = $("#username").val();
        var password = $("#password").val();

        // feedback verifying login
		document.getElementById("verify").innerHTML = authenticate(userName, password);

		// clear input fields
		$("#username").val("");
		$("#password").val("");

    });

});

/*
Authenticates username and password input and returns feedback.
*/
function authenticate(inUsername, inPassword) {
	// get json object
	var retrievedObject = localStorage.getItem('jObject');
	var retrieveO = JSON.parse(retrievedObject);
	
	// iterate through objects to find match
	for (var i = 0; i < retrieveO.length; i++) {
		// if match found
		if (inPassword === retrieveO[i].username) {
			return "Welcome " + inUsername + "!";
		}
	}
	return "Invalid login.";
}
